package ru.kostyan.controller;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.kostyan.model.Role;
import ru.kostyan.model.User;
import ru.kostyan.service.UserService;

import java.util.*;

@Controller
public class UsersController {
    @Autowired
    private UserService userService;
    private Set<String> roles;

    public Set<String> getRoles() {
        roles = Arrays.stream(Role.values()).collect(HashSet::new, (set, e) -> set.add(e.getAuthority()), HashSet::addAll);
        return roles;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView getStartPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/login");
        return modelAndView;
    }

    //получение страницы админа

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView adminPage() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = (User) userService.loadUserByUsername(userDetails.getUsername());
        List<User> userList = userService.getAllUsers();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userList", userList);
        modelAndView.addObject("roleList", Role.values());
        modelAndView.addObject("user", user);
        modelAndView.setViewName("/adminPage");
        return modelAndView;
    }


    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ModelAndView editUser(@ModelAttribute User user,
                                 @RequestParam Map<String, String> form
    ) {
        ModelAndView modelAndView = new ModelAndView();
        Set<Role> thisUserRole = new HashSet<>();
        for (String key :
                form.keySet()) {
            if (getRoles().contains(key)) {
                thisUserRole.add(Role.valueOf(key));
            }
            if (key.equals("active")) {
                user.setActive(true);
            }
        }
        user.setRoles(thisUserRole);
        modelAndView.setViewName("redirect:/admin");
        userService.edit(user);
        return modelAndView;
    }

    //add user post

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addUser(
            @ModelAttribute("user") User user,
            @RequestParam Map<String, String> form) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/admin");
        Set<Role> thisUserRole = new HashSet<>();
        for (String key :
                form.keySet()) {
            if (getRoles().contains(key)) {
                thisUserRole.add(Role.valueOf(key));
            }
        }
        user.setRoles(thisUserRole);
        userService.add(user);
        return modelAndView;
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteUser(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/admin");
        User user = userService.getById(id);
        userService.delete(user);
        return modelAndView;
    }


    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public ModelAndView getMainPage() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = (User) userService.loadUserByUsername(userDetails.getUsername());
        Set<Role> roles = user.getRoles();
        for (Role role : roles) {
            if (role.getAuthority().equals("ADMIN")) {
                user.setAdmin(true);
            }
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("isAdmin", user.isAdmin());
        modelAndView.setViewName("main");
        modelAndView.addObject("user", user);
        return modelAndView;

    }


}
