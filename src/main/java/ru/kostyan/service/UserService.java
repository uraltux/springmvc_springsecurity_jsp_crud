package ru.kostyan.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.kostyan.model.User;

import java.util.List;

public interface UserService extends UserDetailsService{
    void add(User user);
    void delete(User user);
    void edit(User user);
    User getById(Long id);
    List<User> getAllUsers();
}
